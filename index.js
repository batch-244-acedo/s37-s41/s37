const express = require("express");
const mongoose = require("mongoose");

// allows our back-end application to be available to our front-end application
// cross-origin resource sharing
const cors = require("cors");

// allows access to routes defined within our application
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.3zjpe51.mongodb.net/b244_booking?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => 
	console.log("Now connected to the MongoDB Atlas."));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// defines "/users" to be included to all the user routes defined in the userRoutes file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Will use the defined port number for the application whenever an envvironment variable is available OR will use the port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online at ${process.env.PORT || 4000}`)
});