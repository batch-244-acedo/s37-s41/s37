const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course

router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	if (data.isAdmin) {
		courseController.addCourse(data).then(resultFromController => res.send (resultFromController));
	} else {
		res.send (false)
	}
});

// Route for retrieving all the courses
router.get("/allCourses", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Retrieving all ACTIVE courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieving a specific course
router.get("/:courseId", (req, res) => {

	console.log()
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Updating a course
// JWT verification is need to ensure that a user is looged in before updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	// To know if the logged in user is an admin or not
	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))

	// User is not an admin
	} else {
		res.send(false)
	}
	
});

// Route to archive a course


router.patch("/:courseId/archive", auth.verify, (req, res) => {

	// To know if the logged in user is an admin or not
	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
	
});




module.exports = router;